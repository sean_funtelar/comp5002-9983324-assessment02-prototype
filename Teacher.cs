using System.Collections.Generic;

namespace comp5002_9983324_assessment02_prototype
{

    class Teacher : Person
    {
        public Teacher(string name, int id, string userName) : base (name,  id, userName)
        {
        }
        public static string WhatAmITeaching(List<Subjects> subjectsList, string name ) 
        {
            var output = "";
            for (var i =0; i < subjectsList.Count; i++)
            {
                var x = subjectsList [i];
                if (x.teacher == name)
                {
                output += $"{x.teacher} teaches:   {x.courseCode} - {x.courseName}\n";   
                } 
            }
            return output;         
        }            
    }
}




