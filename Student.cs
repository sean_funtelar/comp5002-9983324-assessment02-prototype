using System.Collections.Generic;

namespace comp5002_9983324_assessment02_prototype
{
    


    // TO LINK STUDENT NAME ON RIGHT LINE 
    class Student : Person
    {
        public Student(string name, int id, string userName) : base (name,  id, userName)
        {
        }
        
        //INTRODUCE SUBJECT # FOR STUDENT 
        public static string ListAllMySubjects(List<Subjects> subjectsList, string name ) 
        {
            var output = "";
            foreach (var x in subjectsList)
            {
                output += $"{name} is studying {x.courseCode} - {x.courseName}\n";       
            }
            return output;         
        } 
    }
}

