namespace comp5002_9983324_assessment02_prototype
{
    
    class Subjects
    
    //strings for CourseCode, Course name and teacher 
    {
        private string CourseCode;
        private string CourseName;
        private string Teacher;
        public string courseCode
        {
            get
            {
                return CourseCode;
            }
            set
            {
                CourseCode = value;
            }
        }
        public string courseName
        {
            get
            {
                return CourseName;
            }
            set
            {
                courseName = value;
            }
        }     
        public string teacher
        {
            get
            {
                return Teacher;
            }
            set
            {
                Teacher = value;
            }
        }    

        //Strings teacher name and course to writeline
        public Subjects(string teacher, string courseCode, string courseName)
        {
            Teacher = teacher;
            CourseName = courseName;
            CourseCode = courseCode;
        }
    }
}  