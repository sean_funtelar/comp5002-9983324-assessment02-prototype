﻿using System;
using System.Collections.Generic;

namespace comp5002_9983324_assessment02_prototype
{

    class Program
    {
        static void Main(string[] args)
        {
            //Start the program with Clear();
            
            //STUDENT LISTS TO DISSLAY STUDENT NAME 
            Console.Clear();
            List<Student> studentList = new List<Student>();
            studentList.Add(new Student("Adam Lz", 10008732, "KS19"));
            studentList.Add(new Student("Nicole Lz", 10008782, "LK39"));
            studentList.Add(new Student("James May", 10008726, "MS39"));
            studentList.Add(new Student("Haggard Af Orion", 10008742, "LS11"));
            List<Teacher> teacherList = new List<Teacher>();
            
            //TEACHER LIST TO DISPLAY TEACHER NAME 
            teacherList.Add(new Teacher("Ken Block", 10008726, "KB11"));
            teacherList.Add(new Teacher("Tanner Foust", 10008727, "TF3"));
            teacherList.Add(new Teacher("Jeremy Clarkson", 10008728, "JC12"));
            teacherList.Add(new Teacher("Moog & Marty", 10008729, "MAM13"));
            teacherList.Add(new Teacher("TJ hunt", 10008929, "TJH3"));
            

            //SUBJECT LIST TO DISPLAY CODE, TEACHER NAME AND COURSE NAME
            List<Subjects> subjectsList = new List<Subjects>();
            subjectsList.Add(new Subjects("Ken Block", "GYM101", "How to really do a gymkhana 101"));
            subjectsList.Add(new Subjects("Tanner Foust", "DRI303", "Yes....you can drift a passat 303"));
            subjectsList.Add(new Subjects("Jeremy Clarkson", "TOP101", "How to get sacked on the biggest motoring show on Earth"));
            subjectsList.Add(new Subjects("TJ Hunt", "WAS101", "How to waste money on crap mods on cars"));
            

            //TO DISPLAY TEACHER AND STUDENT INFORMATION
            foreach (var item in teacherList)
            {
            Console.WriteLine(Teacher.WhatAmITeaching(subjectsList, item.name));
            }
            foreach (var item in studentList)
            { 
            Console.WriteLine(Student.ListAllMySubjects(subjectsList, item.name));
            }

            //End the program with blank line and instructions
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Press <Enter> to quit the program");
            Console.ReadKey();
        }       
    }
}